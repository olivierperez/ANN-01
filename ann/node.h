#ifndef NODE_H
#define NODE_H

#include <vector>

#include "link.h"
#include "ann/computer/computer.h"

using namespace std;

class Node
{
public:
    Node();
    ~Node();
    void from(Node *node, double weighting);
    void setValue(const double value);
    double value() const;
    void setComputer(Computer *computer);
    void compute();

private:
    double m_value;
    vector<Link> m_links;
    Computer *m_computer;
};

#endif // NODE_H
