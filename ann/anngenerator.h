#ifndef ANNGENERATOR_H
#define ANNGENERATOR_H

#include "ann.h"

#include <vector>

using namespace std;

class ANNGenerator
{
public:
    ANNGenerator();
    ANNGenerator *withInput(int count);
    ANNGenerator *withOutput(int count);
    ANNGenerator *withLayers(int count, int size);
    ANN generate();
private:
    int m_inputSize;
    int m_outputSize;
    int m_layerCount;
    int m_layerSize;
    vector<vector<vector<double>>> *generateWeighting();
    vector<vector<double> > generateLayerWeighting(int layerSize, int previousLayerSize);
    double randomWeightingValue();
};

#endif // ANNGENERATOR_H
