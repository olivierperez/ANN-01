#ifndef NODESUMMER_H
#define NODESUMMER_H

#include "node.h"



class LinkSummer
{
public:
    LinkSummer():m_sum(0) {}

    void operator()(Link const &link) {
        m_sum += link.value();
    }

    double result() {
        return m_sum;
    }

private:
    double m_sum;
};

#endif // NODESUMMER_H
