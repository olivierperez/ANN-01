#include <algorithm>
#include <iostream>

#include "node.h"
#include "linksummer.h"
#include "ann/computer/computerfactory.h"

using namespace std;

Node::Node() : m_value(0)
{
    m_computer = ComputerFactory::heavisideComputer();
}

Node::~Node()
{}

void Node::from(Node *node, double weighting)
{
    m_links.push_back(Link(node, weighting));
}

void Node::setValue(const double value)
{
    m_value = value;
}

double Node::value() const
{
    return m_value;
}

void Node::setComputer(Computer *computer)
{
    m_computer = computer;
}

void Node::compute()
{
    if (!m_links.empty()) {
        LinkSummer summer;
        summer = for_each(m_links.begin(), m_links.end(), summer);
        //cout << "links: {size: " << std::to_string(m_links.size()) << ", result: " << std::to_string(summer.result()) << "}" << endl;
        m_value = m_computer->compute(summer.result() / m_links.size());
    }
}

