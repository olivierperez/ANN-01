#ifndef ANN_H
#define ANN_H

#include "layer.h"

class ANN
{
public:
    ANN(vector<vector<vector<double>>> *fullweighting);
    ~ANN();
    void addLayer(int size);
    Layer *inputLayer();
    Layer *outputLayer();
    void compute();

private:
    vector<Layer *> m_layers;
    vector<vector<vector<double>>> *m_fullweighting;
    Layer *m_intputLayer;
    Layer *m_outputLayer;
};

#endif // ANN_H
