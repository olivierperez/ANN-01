#include "ann.h"

#include <iostream>

using namespace std;

ANN::ANN(vector<vector<vector<double> > > *fullweighting)
{
    m_fullweighting = fullweighting;
}

ANN::~ANN()
{
    for (vector<Layer*>::iterator it = m_layers.begin(); it != m_layers.end(); ++it) {
        delete (*it);
    }
}

void ANN::addLayer(int size)
{
    Layer *layer = new Layer(size);
    m_layers.push_back(layer);
    if (m_layers.size() >= 2) {
        int previousLayerNum = m_layers.size() - 1 - 1;
        Layer *previousLayer = m_layers[previousLayerNum];
        vector<vector<double>> weighting = (*m_fullweighting)[previousLayerNum];
        layer->link(previousLayer, &weighting);
    }

    if (m_layers.size() == 0) {
        m_intputLayer = layer;
    }
    m_outputLayer = layer;
}

Layer *ANN::inputLayer()
{
    return m_layers[0];
}

Layer *ANN::outputLayer()
{
    return m_layers[m_layers.size()-1];
}

void ANN::compute()
{
    for (vector<Layer*>::iterator it = m_layers.begin(); it != m_layers.end(); ++it) {
        Layer *n = (*it);
        n->compute();
    }
}

