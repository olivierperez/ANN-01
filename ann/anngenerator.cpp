#include "anngenerator.h"
#include <iostream>
#include <random>

using namespace std;

ANNGenerator::ANNGenerator()
{

}

ANNGenerator *ANNGenerator::withInput(int count)
{
    m_inputSize = count;
    return this;
}

ANNGenerator *ANNGenerator::withOutput(int count)
{
    m_outputSize = count;
    return this;
}

ANNGenerator *ANNGenerator::withLayers(int count, int size)
{
    m_layerCount = count;
    m_layerSize = size;
    return this;
}

ANN ANNGenerator::generate()
{
    ANN ann(generateWeighting());
    //cout << "Adding input layer" << endl;
    ann.addLayer(m_inputSize);
    for (int i = 0; i < m_layerCount; i++ ) {
        //cout << "Adding inner layer n" << i+1 << endl;
        ann.addLayer(m_layerSize);
    }
    //cout << "Adding output layer" << endl;
    ann.addLayer(m_outputSize);
    return ann;
}

vector<vector<vector<double> > > *ANNGenerator::generateWeighting()
{
    vector<vector<vector<double> > > *weighting = new vector<vector<vector<double> > >;
    // First inner layou (linked to input layer)
    weighting->push_back(generateLayerWeighting(m_layerSize, m_inputSize));

    // Other inner layouts
    for (int j = 1; j < m_layerCount; j++) {
        weighting->push_back(generateLayerWeighting(m_layerSize, m_layerSize));
    }

    // Output layout
    weighting->push_back(generateLayerWeighting(m_outputSize, m_layerSize));

    return weighting;
}

vector<vector<double> > ANNGenerator::generateLayerWeighting(int layerSize, int previousLayerSize) {
    vector<vector<double> > layerWeighting;

    for(int i = 0; i < layerSize; i++) {
        vector<double> nodeWeightings;
        for(int x = 0; x < previousLayerSize; x++) {
            nodeWeightings.push_back(randomWeightingValue());
        }
        layerWeighting.push_back(nodeWeightings);
    }

    return layerWeighting;
}

double ANNGenerator::randomWeightingValue() {
    return ((double)rand() / RAND_MAX) * 2 - 1;
}

