#ifndef LINK_H
#define LINK_H

class Node;

class Link
{
public:
    double value() const;
    Link(){}
    ~Link();
    Link(Node *from, double weighting);
private:
    Node *m_from;
    double m_weighting;
};

#endif // LINK_H
