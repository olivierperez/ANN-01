#include "node.h"
#include "link.h"

#include <iostream>

using namespace std;

Link::Link(Node *from, double weighting) : m_from(from), m_weighting(weighting)
{}

Link::~Link()
{}

double Link::value() const
{
    return m_from->value() * m_weighting;
}
