#ifndef LAYER_H
#define LAYER_H

#include <list>
#include "link.h"
#include "node.h"

using namespace std;

class Layer
{
public:
    Layer(int number);
    ~Layer();
    void link(Layer *layer, vector<vector<double>> *linksValue);
    void add(Node *node);
    void compute();
    vector<Node *> *nodes();

private:
    vector<Node *> m_nodes;
};

#endif // LAYER_H
