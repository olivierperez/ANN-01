#include <math.h>

#include "sigmoidcomputer.h"

SigmoidComputer::SigmoidComputer()
{

}

double SigmoidComputer::compute(double value) const
{
    return 2 / (1 + exp(-2 * value)) - 1;
}
