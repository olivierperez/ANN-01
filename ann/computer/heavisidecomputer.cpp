#include "heavisidecomputer.h"

HeavisideComputer::HeavisideComputer()
{

}

double HeavisideComputer::compute(double value) const {
    return value > 0 ? 1 : value < 0 ? -1 : 0;
}

