#ifndef SIGMOIDCOMPUTER_H
#define SIGMOIDCOMPUTER_H

#include "computer.h"



class SigmoidComputer : public Computer
{
public:
    SigmoidComputer();

    virtual double compute(double value) const;
};

#endif // SIGMOIDCOMPUTER_H
