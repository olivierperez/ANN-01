#ifndef COMPUTER_H
#define COMPUTER_H

class HeavisideComputer;

class Computer
{
public:
    Computer();
    virtual double compute(double value) const = 0;
};

#endif // COMPUTER_H
