#include <math.h>

#include "gaussiancomputer.h"

GaussianComputer::GaussianComputer()
{

}

double GaussianComputer::compute(double value) const
{
    return exp(-(pow(value, 2))/2);
}
