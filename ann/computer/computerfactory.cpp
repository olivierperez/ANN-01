#include "computerfactory.h"

ComputerFactory::ComputerFactory()
{

}

HeavisideComputer *ComputerFactory::heavisideComputer()
{
    static HeavisideComputer *heavisideComputer = new HeavisideComputer;
    return heavisideComputer;
}

SigmoidComputer *ComputerFactory::sigmoidComputer()
{
    static SigmoidComputer *sigmoidComputer = new SigmoidComputer;
    return sigmoidComputer;
}

GaussianComputer *ComputerFactory::gaussianComputer()
{
    static GaussianComputer *gaussianComputer = new GaussianComputer;
    return gaussianComputer;
}
