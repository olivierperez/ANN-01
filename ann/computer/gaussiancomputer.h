#ifndef GAUSSIANCOMPUTER_H
#define GAUSSIANCOMPUTER_H

#include "computer.h"



class GaussianComputer : public Computer
{
public:
    GaussianComputer();

    virtual double compute(double value) const;
};

#endif // GAUSSIANCOMPUTER_H
