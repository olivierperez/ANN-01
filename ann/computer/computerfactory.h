#ifndef COMPUTERFACTORY_H
#define COMPUTERFACTORY_H

#include "gaussiancomputer.h"
#include "heavisidecomputer.h"
#include "sigmoidcomputer.h"



class ComputerFactory
{
public:
    ComputerFactory();

    static HeavisideComputer *heavisideComputer();
    static SigmoidComputer *sigmoidComputer();
    static GaussianComputer *gaussianComputer();
};

#endif // COMPUTERFACTORY_H
