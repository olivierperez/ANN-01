#ifndef TRINARYCOMPUER_H
#define TRINARYCOMPUER_H

#include "computer.h"



class HeavisideComputer : public Computer
{
public:
    HeavisideComputer();
    virtual double compute(double value) const;
};

#endif // TRINARYCOMPUER_H
