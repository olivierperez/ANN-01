#include <iostream>
#include "layer.h"

Layer::Layer(int number)
{
    m_nodes.assign(number, 0);
    for(int i = 0; i < number; i++) {
        m_nodes[i] = new Node;
    }
}

Layer::~Layer()
{
    for (vector<Node*>::iterator it = m_nodes.begin(); it != m_nodes.end(); it++) {
        delete (*it);
    }
}

void Layer::link(Layer *layer, vector<vector<double>> *linksValue)
{
    vector<Node*> *previousNodes = layer->nodes();

    //cout << "link to previous layer" << endl;
    int i = 0;

    // Loop on nodes of current layer
    for (vector<Node*>::iterator it = m_nodes.begin(); it != m_nodes.end(); it++) {
        //cout << "{node n" << to_string(i) << "}";
        Node *node = *it;
        vector<double> weightings = (*linksValue)[i++];
        int j = 0;

        // Link to nodes of previous layer
        for (vector<Node*>::iterator previousNodeIt = previousNodes->begin(); previousNodeIt != previousNodes->end(); ++previousNodeIt) {
            //cout << "{link n" << to_string(j) << "}";
            node->from(*previousNodeIt, weightings[j++]);
        }
        //cout << endl;
    }
    //cout << "!link to previous layer" << endl << endl;
}

void Layer::add(Node *node)
{
    m_nodes.push_back(node);
}

void Layer::compute()
{
    for (vector<Node*>::iterator it = m_nodes.begin(); it != m_nodes.end(); ++it) {
        Node *n = (*it);
        n->compute();
    }
}

vector<Node *> *Layer::nodes()
{
    return &m_nodes;
}

