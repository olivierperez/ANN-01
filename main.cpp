#include "ann/ann.h"
#include "ann/anngenerator.h"
#include "ann/computer/computerfactory.h"
#include "ann/layer.h"
#include "ann/node.h"

#include <iostream>
#include <list>
#include <time.h>
#include <random>

using namespace std;

int main() {

    // ************
    // build ANN V3
    // ************

    srand(time(0));

    ANNGenerator *generator = new ANNGenerator;
    generator
            ->withInput(2)
            ->withOutput(2)
            ->withLayers(1, 5);
    ANN annGenerated = generator->generate();

    Layer *firstLayer2 = annGenerated.inputLayer();
    Layer *lastLayer2 = annGenerated.outputLayer();

    vector<Node *> firstNodes2 = *firstLayer2->nodes();
    firstNodes2[0]->setValue(0.1);
    firstNodes2[1]->setValue(1);

    vector<Node *> lastNodes2 = *lastLayer2->nodes();
    lastNodes2[0]->setComputer(ComputerFactory::sigmoidComputer());
    lastNodes2[1]->setComputer(ComputerFactory::sigmoidComputer());

    annGenerated.compute();

    cout << "First output: " << lastNodes2[0]->value() << endl;
    cout << "Second output: " << lastNodes2[1]->value() << endl;

    return 0;
}
