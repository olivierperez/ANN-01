CONFIG += c++11

SOURCES += \
    main.cpp \
    ann/node.cpp \
    ann/layer.cpp \
    ann/link.cpp \
    ann/ann.cpp \
    ann/anngenerator.cpp \
    ann/computer/computer.cpp \
    ann/computer/computerfactory.cpp \
    ann/computer/heavisidecomputer.cpp \
    ann/computer/sigmoidcomputer.cpp \
    ann/computer/gaussiancomputer.cpp

HEADERS += \
    ann/node.h \
    ann/layer.h \
    ann/link.h \
    ann/linksummer.h \
    ann/ann.h \
    ann/anngenerator.h \
    ann/computer/computer.h \
    ann/computer/computerfactory.h \
    ann/computer/heavisidecomputer.h \
    ann/computer/sigmoidcomputer.h \
    ann/computer/gaussiancomputer.h
